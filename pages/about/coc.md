---
name: Code of Conduct
---
# Code of Conduct

DebConf is committed to a safe environment for all participants.
All attendees are expected to treat all people and facilities with respect
and help create a welcoming environment. All participants at DebConf must
follow both the [DebConf Code of Conduct][] and the [Debian Code of Conduct][].

[DebConf Code of Conduct]: https://debconf.org/codeofconduct.shtml
[Debian Code of Conduct]: https://www.debian.org/code_of_conduct

If you notice behaviour that fails to meet this standard, please speak up and
help to keep DebConf as respectful as we expect it to be. If you are harassed
and requests to stop are not successful, or notice a disrespectful environment,
the organizers want to help.

You can contact the community team at [community@debian.org][].

We will treat your request with dignity and confidentiality, investigate, and
take whatever actions appropriate. We can provide information on security,
emergency services, transportation, alternative accommodations, or whatever
else may be necessary.

Your security and well-being is our priority, if our intervention is not
successful, DebConf reserves the right to to take action against those who do
not cease unacceptable behaviour.

[community@debian.org]: mailto:community@debian.org

## To Report An Incident

Code of Conduct enforcement and incident response for DebConf 21 will
be handled by members of the Community Team in conjunction with members
of the DebConf 21 orga team.
If you witness a violation of the Code of Conduct (either towards
yourself or someone else), witness other harassment, or are generally
made uncomfortable by a situation and want to talk to someone about it,
do not hesitate to reach out to the Community Team. You can email the
whole team at [community@debian.org][].
Alternatively, you can reach individual team members over email or IRC:

* Steve McIntyre - Sledge - 93sam@d.o - UTC+1 - English - he/him
* Jean-Philippe Mengual - texou - jpmengual@d.o - UTC+2 - English, French - he/him
* Luke W. Faraone - lfaraone - lfaraone@d.o - UTC+2 - English - they/them
* Molly de Blanc - mollydb - mollydb@d.o - UTC-4 - English - she/her
* Pierre-Eliott Bécue - peb - peb@d.o - UTC+2 - English, French - he/him
* Sruthi Chandhan - srud - srud@d.o - UTC+5:30 - English, Hindi, Malayalam  - she/her

When contacting individual team members, please specify if it is okay
for them to share exact or summarized content with the rest of the
Community Team.

We will do our best to be available at all times, however, due to time
zone differences it may take several hours for a response.

## What Counts As An Incident?

Anything that someone feels is in violation of the [DebConf Code of
Conduct][] is an incident. Incidents also include:

* anytime someone is intentionally or unintentionally made to feel
  uncomfortable
* acts of homophobia, racism, sexism, transphobia, and other
  reinforcement of systematic oppression
* unwanted or overly sexual content or imagery
* violent content or imagery
* offensive language around body size, shape, disability, and other
  physical or mental features

## What Counts as DebConf?

This is a tricky question.
Do things people say on Twitter count as DebConf?
What if someone makes a blog post during the conference?
When in doubt, reach out to us anyway and let's talk.

---
title: DebConf21 Moves Online
---

Due to the COVID-19 pandemic, this year's DebConf will once again be
held online.  The exact dates are yet to be determined, and will be
announced later. However, you can expect DebConf21 to happen during the
3rd quarter of 2021, i.e. that period of the year traditionally known as
"DebConf time" :-).

We need a team to make DebConf21 happen. We built some basic
infrastructure for online DebConfs last year, but surely we can do more
this year. If you want to make DebConf special, or just want to help
make it happen, please volunteer and propose your ideas to the
debconf-team@lists.debian.org mailing list.

Among other things, we need volunteers for:

- Design: Logo, website, shirts.
- Content Team: Selecting & scheduling events, coordinating with
  speakers.
- Sponsors Team: Contacting potential sponsors.
- Video Team: Running the video conference sessions and live-mixing the
  streams.
- Overall: Running the event, coordinating with the other debconf teams.

Stay tuned for further news about DebConf21 as they are announced, and for news
about DebConf22+ as decisions are made.

Antonio, for the DebConf team.

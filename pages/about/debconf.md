---
name: About DebConf
---
# About DebConf

DebConf is the annual conference for [Debian](http://debian.org) contributors
and users interested in [improving Debian](https://www.debian.org/intro/help).
[Previous Debian conferences](https://www.debconf.org) have featured speakers
and attendees from all around the world. [DebConf19][] took place in Curitiba,
Brazil and was attended by 382 participants from 50 countries.

[DebConf19]: https://debconf19.debconf.org/

**DebConf21 is taking place Online from August 22 to August 29, 2021.**

It is being preceded by DebCamp, from August 15 to August 21, 2021.

We look forward to seeing you Online!

<form method="POST" action="https://lists.debian.org/cgi-bin/subscribe.pl">
  <fieldset>
    <legend>Register to the debconf-announce mailing list</legend>
    <label for="user_email">Your email address:</label>
    <input name="user_email" size="40" value="" type="Text">
    <input type="hidden" name="list" value="debconf-announce">
    <input name="action" value="Subscribe" type="Submit">
  </fieldset>
</form>

## Codes of Conduct and Community Team

Please see the [Code of Conduct](../coc/) page for more information.

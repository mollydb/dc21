---
name: Important Dates
---
<style type="text/css">
tr td:first-child {
  width: 10rem;
}
</style>


| **APRIL**             |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| tbd.                  | Decision on having the physical conference                           |

| **MAY**               |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| tbd.                  | Opening of the [Call For Proposals](/cfp/) and attendee registration |

| **JULY**              |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| tbd.                  | Last day to register with guaranteed swag. Registrations after this date are still possible, but swag is not guaranteed. |
| tbd.                  | Last day for submitting a talk that will be considered for the official schedule             |


| **AUGUST**            |                                                                      |
|-----------------------|----------------------------------------------------------------------|
| 7 (Saturday)          | Recommended date for presenters to submit pre-recorded talks, so that they can get feedback from the content team. |
| 14 (Saturday)         | Last date for presenters to submit pre-recorded talks to the video team. |
| *DebCamp*             |                                                                      |
| 15 (Sunday)           | First day of DebCamp                                                 |
| 16 (Monday)           | Second day of DebCamp                                                |
| 17 (Tuesday)          | Third day of DebCamp                                                 |
| 18 (Wednesday)        | Fourth day of DebCamp                                                |
| 19 (Thursday)         | Fifth day of DebCamp                                                 |
| 20 (Friday)           | Sixth day of DebCamp                                                 |
| 21 (Saturday)         | Seventh day of DebCamp.                                              |
| *DebConf*             |                                                                      |
| 22 (Sunday)           | First day of DebConf / opening ceremony /                            |
| 23 (Monday)           | Second day of DebConf / evening: cheese and wine party  (tbc.)       |
| 24 (Tuesday)          | Third day of DebConf                                                 |
| 25 (Wednesday)        | Fourth day of DebConf /                                              |
| 26 (Thursday)         | Fifth day of DebConf /                                               |
| 27 (Friday)           | Sixth day of DebConf                                                 |
| 28 (Saturday)         | Last day of DebConf / closing ceremony / teardown                    |
| 29 (Sunday)           | Regular life resumes                                                 |

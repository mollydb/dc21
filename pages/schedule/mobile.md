---
name: Mobile-friendly Schedule
---
# Mobile-friendly Schedule

The schedule should be viewable on a mobile device.
But there are also alternative exports you can use to consume it from
another application.

## XML:

The Schedule is available through an XML feed.
You can use _ConfClerk_ in Debian to consume this, or _Giggity_ on
Android.

<img class="img-fluid" src="{% static "img/dc21-schedule-qr-code.png" %}"
     title="DebConf21 Schedule">

XML Feed: [https://debconf21.debconf.org/schedule/pentabarf.xml](/schedule/pentabarf.xml)

### Download Giggity

- Home page: [https://gaa.st/giggity](https://gaa.st/giggity)
- F-Droid: [https://f-droid.org/repository/browse/?fdid=net.gaast.giggity](https://f-droid.org/repository/browse/?fdid=net.gaast.giggity)
- Google Play Store: [https://play.google.com/store/apps/details?id=net.gaast.giggity
](https://play.google.com/store/apps/details?id=net.gaast.giggity)

### Download ConfClerk

	# apt install confclerk

## iCalendar:

The schedule is available through an iCalendar (ical) feed.
Your calendar application should be able to consume this.

iCalendar Feed: [https://debconf21.debconf.org/schedule/schedule.ics](/schedule/schedule.ics)
